$(document).ready(function () {
    //fetch award data
    $.getJSON('http://localhost:8080/awards', function (data) {
        //loop through and append html to document
        var alternate = false;
        $.each(data, function (key, value) {
            //build row with alternate color
            if(alternate)
            {
                $('#tableBody').append('<tr class="alternate"><td>' + value['year'] + '</td>' +
                    '<td>' + value['actress'] + '</td>' +
                    '<td>' + value['movie'] + '</td></tr>');
            }
            else
            {
                $('#tableBody').append('<tr><td>' + value['year'] + '</td>' +
                    '<td>' + value['actress'] + '</td>' +
                    '<td>' + value['movie'] + '</td></tr>');
            }
            alternate = !alternate;
        });
    });

    //add click to each row only under #tableBody
    $(this).on('click', '#tableBody tr', function () {
        var award = [];
        $(this).children('td').each(function () {
            award.push($(this).html());
        });

        alert('Year: ' + award[0] +
            '\nActress: ' + award[1] +
            '\nMovie: ' + award[2]);
    });
});
