package util;

public class Award
{
    private String year;
    private String actress;
    private String movie;

    public Award(String year, String actress, String movie)
    {
        this.year = year;
        this.actress = actress;
        this.movie = movie;
    }

    public String getYear()
    {
        return year;
    }

    public String getActress()
    {
        return actress;
    }

    public String getMovie()
    {
        return movie;
    }
}
