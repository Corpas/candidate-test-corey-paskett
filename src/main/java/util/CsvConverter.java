package util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvConverter
{
    private String filePath;

    public CsvConverter(String filePath)
    {
        this.filePath = filePath;
    }

    public List<Award> getAwardList()
    {
        List<Award> awards = new ArrayList<>();

        try
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(filePath)));

            String line;
            //skip the first line
            in.readLine();

            while((line = in.readLine()) != null)
            {
                //remove quotes
                line = line.replace("\"", "");
                //split the string on the comma
                String[] awardArr = line.split(",");
                awards.add(new Award(awardArr[0], awardArr[1], awardArr[2]));
            }
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        return awards;
    }
}
