package paskett.corey.CandidateTest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.Award;
import util.CsvConverter;

import java.util.List;

//end point serving the academy award list as json
@RestController
public class AwardsController
{
    @RequestMapping("/awards")
    public List<Award> getAwards()
    {
        CsvConverter csv = new CsvConverter("/csv/academy_award_actresses.csv");

        return csv.getAwardList();
    }
}
