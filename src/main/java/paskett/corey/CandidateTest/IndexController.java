package paskett.corey.CandidateTest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

//serves my index.html
@Controller
public class IndexController
{
    @RequestMapping("/")
    public String getIndex()
    {
        return "index";
    }
}
